require("lint").linters_by_ft = {
  javascript = { "eslint_d", "cspell" },
  astro = { "eslint_d", "stylelint", "cspell" },
  markdown = { "markdownlint", "cspell" },
  haskell = { "hlint", "cspell" },
}

local function to_lint()
  require("lint").try_lint()
end

vim.api.nvim_create_autocmd({ "TextChanged" }, {
  callback = to_lint,
})

vim.api.nvim_create_autocmd({ "BufWritePost" }, {
  callback = to_lint,
})

vim.api.nvim_create_autocmd({ "BufWinEnter" }, {
  callback = to_lint,
})
